import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../util/env";
import {Model} from "../models/model";
import {NamedModel} from "../models/namedModel";

/**
 * Copied from demo project
 */
export abstract class BaseService {

    protected backendUrl!: string;
    protected readonly downloader: HttpClient;

    protected constructor(downloader: HttpClient) {
        this.downloader = downloader;
    }

    public getList(): Observable<Model[]> {
        return this.downloader.get<Model[]>(environment.backendBaseUrl + this.backendUrl + "/fetch");
    }

    public findByName(name: string): Observable<NamedModel> {
        return this.downloader.get<NamedModel>(environment.backendBaseUrl + this.backendUrl + '/search', {params: {name: name}});
    }

    public update(entry: Model): Observable<Model> {
        return this.downloader.put<Model>(environment.backendBaseUrl + this.backendUrl + '/update', entry);
    }

    public save(entry: Model): Observable<Model> {
        return this.downloader.post<Model>(environment.backendBaseUrl + this.backendUrl + "/create", entry);
    }

    public delete(id: number): Observable<HttpResponse<string>> {
        return this.downloader.delete<string>(environment.backendBaseUrl + this.backendUrl + `/delete`, {
            observe: 'response',
            params: {id: id}
        });
    }
}