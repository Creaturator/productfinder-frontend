import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BaseService} from "./base.service";
import {Observable} from "rxjs";
import {Model} from "../models/model";
import {Category} from "../models/category";

@Injectable({
    providedIn: 'root'
})
export class CategoryService extends BaseService {

    constructor(downloader: HttpClient) {
        super(downloader);
        this.backendUrl = 'admin/category';
    }

    public override getList(): Observable<Category[]> {
        return super.getList() as Observable<Category[]>;
    }

    public override findByName(name: string): Observable<Category> {
        return super.findByName(name) as Observable<Category>;
    }

    public override update(entry: Model): Observable<Category> {
        return super.update(entry) as Observable<Category>;
    }

    public override save(entry: Model): Observable<Category> {
        return super.save(entry) as Observable<Category>;
    }

}