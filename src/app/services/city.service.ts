import {Injectable} from "@angular/core";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {BaseService} from "./base.service";
import {Observable} from "rxjs";
import {Model} from "../models/model";
import {City} from "../models/city";
import {environment} from "../util/env";

@Injectable({
    providedIn: 'root'
})
export class CityService extends BaseService {

    constructor(downloader: HttpClient) {
        super(downloader);
        this.backendUrl = 'admin/city';
    }

    public override getList(): Observable<City[]> {
        return this.downloader.get<City[]>(environment.backendBaseUrl + this.backendUrl + "/fetch", {
            params: {
                start: 0,
                amount: 20
            }
        });
    }

    public override findByName(name: string): Observable<City> {
        return super.findByName(name) as Observable<City>;
    }

    public override update(entry: Model): Observable<City> {
        return super.update(entry) as Observable<City>;
    }

    public override save(entry: Model): Observable<City> {
        return super.save(entry) as Observable<City>;
    }

    public fill(formData: FormData): Observable<HttpResponse<string>> {
        throw new Error('Not implemented yet');
    }

}