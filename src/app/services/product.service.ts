import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BaseService} from "./base.service";
import {Observable} from "rxjs";
import {Model} from "../models/model";
import {Product} from "../models/product";

@Injectable({
    providedIn: 'root'
})
export class ProductService extends BaseService {

    constructor(downloader: HttpClient) {
        super(downloader);
        this.backendUrl = 'product';
    }

    public override getList(): Observable<Product[]> {
        return super.getList() as Observable<Product[]>;
    }

    public override findByName(name: string): Observable<Product> {
        return super.findByName(name) as Observable<Product>;
    }

    public override update(entry: Model): Observable<Product> {
        return super.update(entry) as Observable<Product>;
    }

    public override save(entry: Model): Observable<Product> {
        return super.save(entry) as Observable<Product>;
    }

}