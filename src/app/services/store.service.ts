import {Injectable} from "@angular/core";
import {BaseService} from "./base.service";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Model} from "../models/model";
import {Store} from "../models/store";
import {City} from "../models/city";

@Injectable({
    providedIn: 'root'
})
export class StoreService extends BaseService {

    constructor(downloader: HttpClient) {
        super(downloader);
        this.backendUrl = 'store';
    }

    public static getFilteringPredicate(city: City): (data: Store) => boolean {
        return (data: Store) => data.city === city;
    }

    public override getList(): Observable<Store[]> {
        return super.getList() as Observable<Store[]>;
    }

    public override findByName(name: string): Observable<Store> {
        return super.findByName(name) as Observable<Store>;
    }

    public override update(entry: Model): Observable<Store> {
        return super.update(entry) as Observable<Store>;
    }

    public override save(entry: Model): Observable<Store> {
        return super.save(entry) as Observable<Store>;
    }

}