import {Injectable} from "@angular/core";
import {BaseService} from "./base.service";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Model} from "../models/model";
import {Stock} from "../models/stock";
import {City} from "../models/city";
import {StoreService} from "./store.service";
import {NamedModel} from "../models/namedModel";

@Injectable({
    providedIn: 'root'
})
export class StockService extends BaseService {

    constructor(downloader: HttpClient) {
        super(downloader);
        this.backendUrl = 'admin/stock';
    }

    public static getFilteringPredicate(city: City): (data: Stock) => boolean {
        return (data: Stock) => StoreService.getFilteringPredicate(city).call(this, data.store);
    }

    public override getList(): Observable<Stock[]> {
        return super.getList() as Observable<Stock[]>;
    }

    public override findByName(name: string): Observable<NamedModel> {
        throw new Error('Fetching one does not exist for stocks');
    }

    public override update(entry: Model): Observable<Stock> {
        return super.update(entry) as Observable<Stock>;
    }

    public override save(entry: Model): Observable<Stock> {
        return super.save(entry) as Observable<Stock>;
    }

}