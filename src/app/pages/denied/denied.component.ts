import {Component} from '@angular/core';
import {BaseComponent} from "../../components/base/base-component/base.component";

@Component({
    selector: 'app-denied',
    templateUrl: './denied.component.html',
    styleUrl: './denied.component.less'
})
export class DeniedComponent extends BaseComponent {

}
