import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {storageFactory} from "../../modules/app.module"
import {CityService} from "../../services/city.service";
import {BaseComponent} from "../../components/base/base-component/base.component";

@Component({
    selector: 'app-city-search',
    templateUrl: './city-search.component.html',
    styleUrl: './city-search.component.less'
})
export class CitySearchComponent extends BaseComponent {

    public static readonly cityCacheKey: string = 'city';

    private router: Router;
    private cityService: CityService;

    constructor(router: Router, cityService: CityService) {
        super();
        this.router = router;
        this.cityService = cityService;
    }

    protected passNameToProductsPage(event: SubmitEvent): void {
        event.preventDefault();
        event.stopPropagation();
        this.cityService.findByName((document.getElementById('locationInput') as HTMLInputElement).value)
            .subscribe(city => {
                storageFactory().setItem(CitySearchComponent.cityCacheKey, JSON.stringify(city));
            });
        this.router.navigateByUrl('product/search');
    }

}
