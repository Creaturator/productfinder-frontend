import {Component} from '@angular/core';
import {Category} from "../../../models/category";
import {CategoryService} from "../../../services/category.service";
import {ProductService} from "../../../services/product.service";
import {Product} from "../../../models/product";
import {NamedModel} from "../../../models/namedModel";
import {BaseComponent} from "../../../components/base/base-component/base.component";

@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrl: './product-list.component.less'
})
export class ProductListComponent extends BaseComponent {

    public columns: string[] = ['name', 'organic', 'nutri-score', 'category', 'buttons'];
    public readonly menuEntries: string[][] = [["Stock", "/product/search", "Stock"], ["Categories", "/category/list", "Category"]];

    protected categories: Category[] = [];
    protected products: Product[] = [];
    private productService: ProductService;

    constructor(productService: ProductService, categoryService: CategoryService) {
        super();
        this.productService = productService;
        productService.getList().subscribe(data => this.products = data);
        categoryService.getList().subscribe(data => this.categories = data);
    }

    protected async lookupProduct(name: string): Promise<string[]> {
        return await this.productService.getList().toPromise().then(c => c!.filter(ca => ca.name.includes(name)).map(ca => ca.name));
    }

    protected filterProducts(filters: NamedModel[]): void {
        throw new Error('Not yet implemented');
    }

}
