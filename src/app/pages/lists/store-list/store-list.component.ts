import {Component} from '@angular/core';
import {Store} from "../../../models/store";
import {StoreService} from "../../../services/store.service";
import {City} from "../../../models/city";
import {CityService} from "../../../services/city.service";
import {NamedModel} from "../../../models/namedModel";
import {Router} from "@angular/router";
import {storageFactory} from "../../../modules/app.module";
import {CitySearchComponent} from "../../city-search/city-search.component";
import {BaseComponent} from "../../../components/base/base-component/base.component";

@Component({
    selector: 'app-store-list',
    templateUrl: './store-list.component.html',
    styleUrl: './store-list.component.less'
})
export class StoreListComponent extends BaseComponent {

    public curCity: City | null = null;
    public columns: string[] = ['name', 'address', 'city', 'buttons'];
    public readonly menuEntries: string[][] = [["Products", "/product/list", "Products"], ["Stock", "/product/search", "Stock"]];

    protected stores: Store[] = [];
    protected cities: City[] = [];
    private storeService: StoreService;

    constructor(router: Router, storeService: StoreService, cityService: CityService) {
        super();
        this.storeService = storeService;
        if (storageFactory().getItem(CitySearchComponent.cityCacheKey))
            this.curCity = JSON.parse(storageFactory().getItem(CitySearchComponent.cityCacheKey)!);
        else {
            router.navigateByUrl('citySearch');
            return;
        }
        storeService.getList().subscribe(data => {
            this.stores = data;
            this.filterModeldata()
        });
        cityService.getList().subscribe(data => this.cities = data);
    }

    protected async lookupStore(name: string): Promise<string[]> {
        return await this.storeService.getList().toPromise().then(s => s!.filter(st => st.name.includes(name)).map(st => st.name));
    }

    protected filterStores(filters: NamedModel[]): void {
        throw new Error('Not yet implemented');
    }

    private filterModeldata(): void {
        if (this.curCity === null || this.stores === null)
            return;
        this.stores!.filter(s => StoreService.getFilteringPredicate(this.curCity!).call(this, s));
    }

}
