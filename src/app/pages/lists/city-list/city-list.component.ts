import {Component} from '@angular/core';
import {City} from "../../../models/city";
import {CityService} from "../../../services/city.service";
import {BaseComponent} from "../../../components/base/base-component/base.component";

@Component({
    selector: 'app-city-list',
    templateUrl: './city-list.component.html',
    styleUrl: './city-list.component.less'
})
export class CityListComponent extends BaseComponent {

    public columns: string[] = ['name', 'alternate-names'];
    public readonly menuEntries: string[][] = [["Stores", "/store/list", "Shop"]];
    protected cities: City[] = [];
    private cityService: CityService;

    constructor(cityService: CityService) {
        super();
        this.cityService = cityService;
        cityService.getList().subscribe(data => {
            this.cities = data;
            this.splitAlternateNames(data)
        });
    }

    protected async lookupCity(name: string): Promise<string[]> {
        return await this.cityService.getList().toPromise().then(c => c!.filter(ci => ci.name.includes(name)).map(ci => ci.name));
    }

    private splitAlternateNames(cities: City[]): void {
        for (let city of cities)
            city.alternateNameArray = city.alternateNames.split(',');
    }

}
