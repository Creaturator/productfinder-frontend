import {Component} from '@angular/core';
import {CategoryService} from "../../../services/category.service";
import {Category} from "../../../models/category";
import {BaseComponent} from "../../../components/base/base-component/base.component";

@Component({
    selector: 'app-category-list',
    templateUrl: './category-list.component.html',
    styleUrl: './category-list.component.less'
})
export class CategoryListComponent extends BaseComponent {

    public columns: string[] = ['name', 'vegan', 'buttons'];
    public readonly menuEntries: string[][] = [["Products", "/product/list", "Products"], ["Stock", "/product/search", "Stock"]];

    protected categories: Category[] = [];
    private categoryService: CategoryService;

    constructor(categoryService: CategoryService) {
        super();
        this.categoryService = categoryService;
        categoryService.getList().subscribe(data => this.categories = data);
    }

    protected async lookupCategory(name: string): Promise<string[]> {
        return await this.categoryService.getList().toPromise().then(c => c!.filter(ca => ca.name.includes(name)).map(ca => ca.name));
    }

}
