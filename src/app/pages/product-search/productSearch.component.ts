import {Component} from '@angular/core';
import {BaseComponent} from "../../components/base/base-component/base.component";
import {City} from "../../models/city";
import {Stock} from "../../models/stock";
import {Category} from "../../models/category";
import {NamedModel} from "../../models/namedModel";
import {Router} from "@angular/router";
import {CategoryService} from "../../services/category.service";
import {ProductService} from "../../services/product.service";
import {StoreService} from "../../services/store.service";
import {StockService} from "../../services/stock.service";
import {Store} from "../../models/store";
import {storageFactory} from "../../modules/app.module";
import {CitySearchComponent} from "../city-search/city-search.component";

@Component({
    selector: 'product-search',
    templateUrl: './productSearch.component.html',
    styleUrl: './productSearch.component.less'
})
export class ProductSearchComponent extends BaseComponent {
    public city: City | null = null;
    public productStocks: Stock[] | null = null;
    public categories: Category[] | null = null;
    public stores: Store[] | null = null;

    private readonly productService: ProductService;

    public columns: string[] = ['name', 'category', 'stores', 'buttons'];
    public readonly menuEntries: string[][] = [["Stores", "/store/list", "Shop"], ["Locations", "/city/list", "City"], ["Shopping list", "/shoppingList", "GroceryList"]];

    constructor(router: Router, categoryService: CategoryService, productService: ProductService, storeService: StoreService, stockService: StockService) {
        super();
        this.productService = productService;
        if (storageFactory().getItem(CitySearchComponent.cityCacheKey))
            this.city = JSON.parse(storageFactory().getItem(CitySearchComponent.cityCacheKey)!);
        else {
            router.navigateByUrl('citySearch');
            return;
        }
        stockService.getList().subscribe(stocks => {
            this.productStocks = stocks;
            this.filterModeldata()
        });
        categoryService.getList().subscribe(data => this.categories = data);
        storeService.getList().subscribe(data => {
            this.stores = data;
            this.filterModeldata()
        });

    }

    protected async lookupProduct(name: string): Promise<string[]> {
        return await this.productService.getList().toPromise().then(p => p!.filter(pr => pr.name.includes(name)).map(p => p.name));
    }

    protected filterProducts(filters: NamedModel[]): void {
        throw new Error('Not yet implemented');
    }

    private filterModeldata(): void {
        if (this.city === null || this.productStocks === null || this.stores === null)
            return;
        this.productStocks!.filter(s => StockService.getFilteringPredicate(this.city!).call(this, s));
        this.stores!.filter(s => StoreService.getFilteringPredicate(this.city!).call(this, s));
    }

}
