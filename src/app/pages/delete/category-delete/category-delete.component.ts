import {Component} from '@angular/core';
import {Category} from "../../../models/category";
import {CategoryService} from "../../../services/category.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-category-delete',
    templateUrl: './category-delete.component.html',
    styleUrl: './category-delete.component.less'
})
export class CategoryDeleteComponent {

    protected category!: Category;
    protected categoryService: CategoryService;

    constructor(routeInfo: ActivatedRoute, categoryService: CategoryService, router: Router) {
        this.categoryService = categoryService;
        routeInfo.queryParamMap.subscribe(map => {
            if (map.has('name')) {
                categoryService.findByName(map.get('name')).subscribe(data => {
                    this.category = data;
                });
            } else
                router.navigateByUrl('category/list');
        })
    }

    protected deleteItem(): void {
        this.categoryService.delete(this.category.id);
    }

}
