import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Stock} from "../../../models/stock";
import {StockService} from "../../../services/stock.service";

@Component({
    selector: 'app-stock-delete',
    templateUrl: './stock-delete.component.html',
    styleUrl: './stock-delete.component.less'
})
export class StockDeleteComponent {
    protected city!: Stock;
    protected cityService: StockService;

    constructor(routeInfo: ActivatedRoute, cityService: StockService, router: Router) {
        this.cityService = cityService;
        routeInfo.queryParamMap.subscribe(map => {
            if (map.has('name')) {

            } else
                router.navigateByUrl('stock/list');
        })
    }

    protected deleteItem(): void {
        this.cityService.delete(this.city.id);
    }
}
