import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {City} from "../../../models/city";
import {CityService} from "../../../services/city.service";

@Component({
    selector: 'app-city-delete',
    templateUrl: './city-delete.component.html',
    styleUrl: './city-delete.component.less'
})
export class CityDeleteComponent {
    protected city!: City;
    protected cityService: CityService;

    constructor(routeInfo: ActivatedRoute, cityService: CityService, router: Router) {
        this.cityService = cityService;
        routeInfo.queryParamMap.subscribe(map => {
            if (map.has('name')) {
                cityService.findByName(map.get('name')).subscribe(data => {
                    this.city = data;
                });
            } else
                router.navigateByUrl('city/list');
        })
    }

    protected deleteItem(): void {
        this.cityService.delete(this.city.id);
    }
}
