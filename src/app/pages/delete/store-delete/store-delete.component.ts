import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "../../../models/store";
import {StoreService} from "../../../services/store.service";

@Component({
    selector: 'app-store-delete',
    templateUrl: './store-delete.component.html',
    styleUrl: './store-delete.component.less'
})
export class StoreDeleteComponent {
    protected city!: Store;
    protected cityService: StoreService;

    constructor(routeInfo: ActivatedRoute, cityService: StoreService, router: Router) {
        this.cityService = cityService;
        routeInfo.queryParamMap.subscribe(map => {
            if (map.has('name')) {
                cityService.findByName(map.get('name')).subscribe(data => {
                    this.city = data;
                });
            } else
                router.navigateByUrl('store/list');
        })
    }

    protected deleteItem(): void {
        this.cityService.delete(this.city.id);
    }
}
