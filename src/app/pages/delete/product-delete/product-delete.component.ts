import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Product} from "../../../models/product";
import {ProductService} from "../../../services/product.service";

@Component({
    selector: 'app-product-delete',
    templateUrl: './product-delete.component.html',
    styleUrl: './product-delete.component.less'
})
export class ProductDeleteComponent {
    protected city!: Product;
    protected cityService: ProductService;

    constructor(routeInfo: ActivatedRoute, cityService: ProductService, router: Router) {
        this.cityService = cityService;
        routeInfo.queryParamMap.subscribe(map => {
            if (map.has('name')) {
                cityService.findByName(map.get('name')).subscribe(data => {
                    this.city = data;
                });
            } else
                router.navigateByUrl('product/list');
        })
    }

    protected deleteItem(): void {
        this.cityService.delete(this.city.id);
    }
}
