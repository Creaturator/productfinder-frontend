import {Component} from '@angular/core';
import {BaseComponent} from "../../../components/base/base-component/base.component";
import {Category} from "../../../models/category";
import {DetailsValuesHolder} from "../../../util/DetailsValuesHolder";
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from "../../../services/category.service";

@Component({
    selector: 'app-category-details',
    templateUrl: './category-details.component.html',
    styleUrl: './category-details.component.less'
})
export class CategoryDetailsComponent extends BaseComponent {

    protected category!: Category;

    protected categoryFields: DetailsValuesHolder[] | null = null;

    constructor(routeInfo: ActivatedRoute, router: Router, categoryService: CategoryService) {
        super();
        routeInfo.queryParamMap.subscribe(params => {
            if (params.has('name')) {
                categoryService.findByName(params.get('name')!).subscribe(data => {
                    this.category = data;
                    this.categoryFields = [
                        new DetailsValuesHolder('name', 'Category name', this.category.name),
                        new DetailsValuesHolder('vegan', 'Does it consist of vegan food?', this.category.vegan ? 'Yes' : 'No'),
                    ];
                });
            } else {
                router.navigateByUrl('category/list');
            }
        });
    }

}
