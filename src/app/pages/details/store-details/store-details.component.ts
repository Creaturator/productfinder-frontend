import {Component} from '@angular/core';
import {BaseComponent} from "../../../components/base/base-component/base.component";
import {DetailsValuesHolder} from "../../../util/DetailsValuesHolder";
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "../../../models/store";
import {StoreService} from "../../../services/store.service";

@Component({
    selector: 'app-store-details',
    templateUrl: './store-details.component.html',
    styleUrl: './store-details.component.less'
})
export class StoreDetailsComponent extends BaseComponent {

    protected store!: Store;

    protected storeFields: DetailsValuesHolder[] | null = null;

    constructor(routeInfo: ActivatedRoute, router: Router, storeService: StoreService) {
        super();
        routeInfo.queryParamMap.subscribe(params => {
            if (params.has('name')) {
                storeService.findByName(params.get('name')!).subscribe(data => {
                    this.store = data;
                    this.storeFields = [
                        new DetailsValuesHolder('name', 'Product name', this.store.name),
                        new DetailsValuesHolder('address', 'The address of the store', this.store.address),
                        new DetailsValuesHolder('city', 'In which city is the store located', this.store.city.name),
                    ];
                });
            } else {
                router.navigateByUrl('store/list');
            }
        });
    }

}
