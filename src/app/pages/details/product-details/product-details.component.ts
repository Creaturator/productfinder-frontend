import {Component} from '@angular/core';
import {BaseComponent} from "../../../components/base/base-component/base.component";
import {DetailsValuesHolder} from "../../../util/DetailsValuesHolder";
import {ActivatedRoute, Router} from "@angular/router";
import {Product} from "../../../models/product";
import {ProductService} from "../../../services/product.service";

@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.component.html',
    styleUrl: './product-details.component.less'
})
export class ProductDetailsComponent extends BaseComponent {
    protected product!: Product;

    protected productFields: DetailsValuesHolder[] | null = null;

    constructor(routeInfo: ActivatedRoute, router: Router, productService: ProductService) {
        super();
        routeInfo.queryParamMap.subscribe(params => {
            if (params.has('name')) {
                productService.findByName(params.get('name')!).subscribe(data => {
                    this.product = data;
                    this.productFields = [
                        new DetailsValuesHolder('name', 'Product name', this.product.name),
                        new DetailsValuesHolder('organic', 'Is this item organic?', this.product.organic ? 'Yes' : 'No'),
                        new DetailsValuesHolder('nutri-score', 'How healthy is this product from a to f', this.product.nutriScore),
                        new DetailsValuesHolder('price', 'The price of this product', this.product.price),
                        new DetailsValuesHolder('category', 'The category this product falls into', this.product.category.name),
                    ];
                });
            } else {
                router.navigateByUrl('product/list');
            }
        });
    }
}
