import {Component} from '@angular/core';
import {BaseComponent} from "../../../components/base/base-component/base.component";
import {DetailsValuesHolder} from "../../../util/DetailsValuesHolder";
import {ActivatedRoute, Router} from "@angular/router";
import {City} from "../../../models/city";
import {CityService} from "../../../services/city.service";

@Component({
    selector: 'app-city-details',
    templateUrl: './city-details.component.html',
    styleUrl: './city-details.component.less'
})
export class CityDetailsComponent extends BaseComponent {
    protected city!: City;

    protected cityFields: DetailsValuesHolder[] | null = null;

    constructor(routeInfo: ActivatedRoute, router: Router, cityService: CityService) {
        super();
        routeInfo.queryParamMap.subscribe(params => {
            if (params.has('name')) {
                cityService.findByName(params.get('name')!).subscribe(data => {
                    this.city = data;
                    this.cityFields = [
                        new DetailsValuesHolder('name', 'City name', this.city.name),
                        new DetailsValuesHolder('alternate-names', 'All alternative names for this city', this.city.alternateNames),
                    ];
                });
            } else {
                router.navigateByUrl('city/list');
            }
        });
    }
}
