import {Component} from '@angular/core';
import {BaseComponent} from "../../../components/base/base-component/base.component";
import {DetailsValuesHolder} from "../../../util/DetailsValuesHolder";
import {ActivatedRoute, Router} from "@angular/router";
import {Stock} from "../../../models/stock";
import {StockService} from "../../../services/stock.service";

@Component({
    selector: 'stock-details',
    templateUrl: './stock-details.component.html',
    styleUrl: './stock-details.component.less'
})
export class StockDetailsComponent extends BaseComponent {

    protected stock!: Stock;

    protected stockFields: DetailsValuesHolder[] | null = null;

    constructor(routeInfo: ActivatedRoute, router: Router, stockService: StockService) {
        super();
        routeInfo.queryParamMap.subscribe(params => {
            if (params.has('name')) {
                stockService.getList().subscribe(data => {
                    data = data.filter(d => d.product.name === params.get('name')!);
                    this.stock = data[0];
                    this.stockFields = [
                        new DetailsValuesHolder('productName', 'The product in stock', this.stock.product.name),
                        new DetailsValuesHolder('price', 'The price at the given store for the given product', this.stock.price),
                        new DetailsValuesHolder('discount', 'The discount on this product', this.stock.discount),
                        new DetailsValuesHolder('confirms', 'How many people agreed that this entry is correct', this.stock.confirmationAmount),
                        new DetailsValuesHolder('storeName', 'The store where to find this stocked product', this.stock.store.name),
                    ];
                });
            } else {
                router.navigateByUrl('stock/list');
            }
        });
    }

}
