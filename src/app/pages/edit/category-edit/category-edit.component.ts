import {Component} from '@angular/core';
import {Category} from "../../../models/category";
import {ActivatedRoute} from "@angular/router";
import {CategoryService} from "../../../services/category.service";
import {BaseComponent} from "../../../components/base/base-component/base.component";
import {EditValuesHolder} from "../../../util/EditValuesHodler";

@Component({
    selector: 'app-category-edit',
    templateUrl: './category-edit.component.html',
    styleUrl: './category-edit.component.less'
})
export class CategoryEditComponent extends BaseComponent {

    protected category!: Category;
    protected categoryService: CategoryService;

    protected categoryFields: EditValuesHolder[] | null = null;

    constructor(routeInfo: ActivatedRoute, categoryService: CategoryService) {
        super();
        this.categoryService = categoryService;
        routeInfo.queryParamMap.subscribe(params => {
            if (params.has('name')) {
                categoryService.findByName(params.get('name')!).subscribe(data => {
                    this.category = data;
                    this.categoryFields = [
                        new EditValuesHolder('name', 'Category name', this.category.name, 'text'),
                        new EditValuesHolder('vegan', 'Does it consist of vegan food?', this.category.vegan, 'checkbox'),
                    ];
                });
            } else {
                this.category = new Category();
                this.categoryFields = [
                    new EditValuesHolder('name', 'Category name', this.category.name, 'text'),
                    new EditValuesHolder('vegan', 'Does it consist of vegan food?', this.category.vegan, 'checkbox'),
                ];
            }
        });
    }

    protected saveChanges(node: EventTarget): void {
        let form = node as HTMLFormElement;
        let category = new Category();
        category.name = (form.querySelector('#name')! as HTMLInputElement).value;
        category.vegan = parseInt((form.querySelector('#vegan')! as HTMLInputElement).value) === 1;
        this.categoryService.save(category);
    }

}
