import {Component} from '@angular/core';
import {BaseComponent} from "../../../components/base/base-component/base.component";
import {Product} from "../../../models/product";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../../../services/product.service";
import {CategoryService} from "../../../services/category.service";
import {EditValuesHolder} from "../../../util/EditValuesHodler";

@Component({
    selector: 'app-product-edit',
    templateUrl: './product-edit.component.html',
    styleUrl: './product-edit.component.less'
})
export class ProductEditComponent extends BaseComponent {
    protected product!: Product;
    protected productFields: EditValuesHolder[] | null = null;
    private productService: ProductService;
    private categoryService: CategoryService;

    constructor(routeInfo: ActivatedRoute, router: Router, productService: ProductService, categoryService: CategoryService) {
        super();
        this.productService = productService;
        this.categoryService = categoryService;
        routeInfo.queryParamMap.subscribe(params => {
            if (params.has('name')) {
                productService.findByName(params.get('name')!).subscribe(data => {
                    this.product = data;
                    this.productFields = [
                        new EditValuesHolder('name', 'Product name', this.product.name, 'text'),
                        new EditValuesHolder('organic', 'Is this item organic?', this.product.organic, 'checkbox'),
                        new EditValuesHolder('nutri-score', 'How healthy is this product from a to f', this.product.nutriScore, 'text'),
                        new EditValuesHolder('price', 'The price of this product', this.product.price, 'number'),
                        new EditValuesHolder('category', 'The category this product falls into', this.product.category.name, 'text'),
                    ];
                });
            } else {
                router.navigateByUrl('product/list');
            }
        });
    }

    protected saveChanges(node: EventTarget): void {
        let form = node as HTMLFormElement;
        this.categoryService.findByName((form.querySelector('#alternate-names')! as HTMLInputElement).value).subscribe(data => {
            let city = new Product();
            city.name = (form.querySelector('#name')! as HTMLInputElement).value;
            city.category = data;
            this.productService.save(city);
        });
    }

}
