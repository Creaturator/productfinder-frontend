import {Component} from '@angular/core';
import {BaseComponent} from "../../../components/base/base-component/base.component";
import {Stock} from "../../../models/stock";
import {ActivatedRoute, Router} from "@angular/router";
import {StockService} from "../../../services/stock.service";
import {ProductService} from "../../../services/product.service";
import {StoreService} from "../../../services/store.service";
import {EditValuesHolder} from "../../../util/EditValuesHodler";

@Component({
    selector: 'app-stock-edit',
    templateUrl: './stock-edit.component.html',
    styleUrl: './stock-edit.component.less'
})
export class StockEditComponent extends BaseComponent {
    protected stock!: Stock;
    protected stockFields: EditValuesHolder[] | null = null;
    private stockService: StockService;
    private productService: ProductService;
    private storeService: StoreService;

    constructor(routeInfo: ActivatedRoute, router: Router, stockService: StockService, productService: ProductService, storeService: StoreService) {
        super();
        this.stockService = stockService;
        this.productService = productService;
        this.storeService = storeService;
        routeInfo.queryParamMap.subscribe(params => {
            if (params.has('name')) {
                stockService.getList().subscribe(data => {
                    data = data.filter(d => d.product.name === params.get('name')!);
                    this.stock = data[0];
                    this.stockFields = [
                        new EditValuesHolder('productName', 'The product in stock', this.stock.product.name, 'text'),
                        new EditValuesHolder('price', 'The price at the given store for the given product', this.stock.price, 'number'),
                        new EditValuesHolder('discount', 'The discount on this product', this.stock.discount, 'number'),
                        new EditValuesHolder('confirms', 'How many people agreed that this entry is correct', this.stock.confirmationAmount, 'number'),
                        new EditValuesHolder('storeName', 'The store where to find this stocked product', this.stock.store.name, 'text'),
                    ];
                });
            } else {
                router.navigateByUrl('stock/list');
            }
        });
    }

    protected saveChanges(node: EventTarget): void {
        let form = node as HTMLFormElement;
        this.productService.findByName((form.querySelector('#productName')! as HTMLInputElement).value).subscribe(datap => {
            this.storeService.findByName((form.querySelector('#storeName')! as HTMLInputElement).value).subscribe(datas => {
                let stock = new Stock();
                stock.store = datas;
                stock.product = datap;
                stock.confirmationAmount = parseInt((form.querySelector('#confirms')! as HTMLInputElement).value);
                stock.discount = parseInt((form.querySelector('#discount')! as HTMLInputElement).value);
                stock.price = parseInt((form.querySelector('#price')! as HTMLInputElement).value);

                this.stockService.save(stock);
            });
        });
    }
}
