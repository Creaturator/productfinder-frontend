import {Component} from '@angular/core';
import {BaseComponent} from "../../../components/base/base-component/base.component";
import {City} from "../../../models/city";
import {ActivatedRoute, Router} from "@angular/router";
import {CityService} from "../../../services/city.service";
import {EditValuesHolder} from "../../../util/EditValuesHodler";

@Component({
    selector: 'app-city-edit',
    templateUrl: './city-edit.component.html',
    styleUrl: './city-edit.component.less'
})
export class CityEditComponent extends BaseComponent {
  protected city!: City;
  protected cityFields: EditValuesHolder[] | null = null;
  private cityService: CityService;

  constructor(routeInfo: ActivatedRoute, router: Router, cityService: CityService) {
    super();
    this.cityService = cityService;
    routeInfo.queryParamMap.subscribe(params => {
      if (params.has('name')) {
        cityService.findByName(params.get('name')!).subscribe(data => {
          this.city = data;
          this.cityFields = [
            new EditValuesHolder('name', 'City name', this.city.name, 'text'),
            new EditValuesHolder('alternate-names', 'All alternative names for this city', this.city.alternateNames, 'text'),
          ];
        });
      } else {
        this.city = new City();
        this.cityFields = [
          new EditValuesHolder('name', 'City name', this.city.name, 'text'),
          new EditValuesHolder('alternate-names', 'All alternative names for this city', this.city.alternateNames, 'text'),
        ];
      }
    });
  }

  protected saveChanges(node: EventTarget): void {
    let form = node as HTMLFormElement;
    let city = new City();
    city.name = (form.querySelector('#name')! as HTMLInputElement).value;
    city.alternateNames = (form.querySelector('#alternate-names')! as HTMLInputElement).value;
    this.cityService.save(city);
  }

}
