import {Component} from '@angular/core';
import {BaseComponent} from "../../../components/base/base-component/base.component";
import {Store} from "../../../models/store";
import {ActivatedRoute, Router} from "@angular/router";
import {StoreService} from "../../../services/store.service";
import {CityService} from "../../../services/city.service";
import {EditValuesHolder} from "../../../util/EditValuesHodler";

@Component({
    selector: 'app-store-edit',
    templateUrl: './store-edit.component.html',
    styleUrl: './store-edit.component.less'
})
export class StoreEditComponent extends BaseComponent {
    protected store!: Store;
    protected storeFields: EditValuesHolder[] | null = null;
    private storeService: StoreService;
    private cityService: CityService;

    constructor(routeInfo: ActivatedRoute, router: Router, storeService: StoreService, cityService: CityService) {
        super();
        this.storeService = storeService;
        this.cityService = cityService;
        routeInfo.queryParamMap.subscribe(params => {
            if (params.has('name')) {
                storeService.findByName(params.get('name')!).subscribe(data => {
                    this.store = data;
                    this.storeFields = [
                        new EditValuesHolder('name', 'Product name', this.store.name, 'text'),
                        new EditValuesHolder('address', 'The address of the store', this.store.address, 'text'),
                        new EditValuesHolder('city', 'In which city is the store located', this.store.city.name, 'text'),
                    ];
                });
            } else {
                router.navigateByUrl('store/list');
            }
        });
    }

    protected saveChanges(node: EventTarget): void {
        let form = node as HTMLFormElement;
        this.cityService.findByName((form.querySelector('#city')! as HTMLInputElement).value).subscribe(data => {
            let store = new Store();
            store.name = (form.querySelector('#name')! as HTMLInputElement).value;
            store.city = data;
            this.storeService.save(store);
        });
    }

}
