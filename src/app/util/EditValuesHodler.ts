export class EditValuesHolder {

    public id: string;
    public label: string;
    public value: any;
    public type: string;

    constructor(id: string, label: string, value: any, type: string) {
        this.id = id;
        this.label = label;
        this.value = value;
        this.type = type;
    }

}