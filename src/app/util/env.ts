/**
 * Copied from demo project and updated
 */
export const environment = {
    production: false,
    backendBaseUrl: 'http://localhost:9090/api/',
    frontendBaseUrl: 'http://localhost:7070'
};
