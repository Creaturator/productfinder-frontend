import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Router} from '@angular/router';
import {AuthConfig} from 'angular-oauth2-oidc';
import {AppAuthService} from '../../services/app.auth.service';
import {environment} from '../env';

/**
 * COpied from demo project
 */
@Injectable()
export class AppAuthGuard {

  public static readonly authConfig: AuthConfig = {
    issuer: 'http://localhost:8080/realms/Productfinder',
    requireHttps: false,
    redirectUri: environment.frontendBaseUrl,
    postLogoutRedirectUri: environment.frontendBaseUrl,
    clientId: 'springAPI',
    scope: 'openid profile roles offline_access',
    responseType: 'code',
    showDebugInformation: true,
    requestAccessToken: true,
    silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',
    silentRefreshTimeout: 500,
    clearHashAfterLogin: true,
  };

  private userRoles?: string[];

  constructor (
      private authService: AppAuthService,
      private router: Router) {
  }

  canActivate (route: ActivatedRouteSnapshot) {
    // @ts-ignore
    this.authService.getRoles().subscribe(roles => {
      this.userRoles = roles;
    });

    if (this.authService.isAuthenticated()) {
      const hasRoles = this.checkRoles(route);
      if (!hasRoles) {
        return this.router.parseUrl('/noaccess');
      }
      return hasRoles;
    }
    return this.router.parseUrl('/noaccess');
  }

  checkRoles (route: ActivatedRouteSnapshot) : boolean {
    const roles = route.data['roles'] as Array<string>;

    if (roles === undefined || roles === null || roles.length === 0) {
      return true;
    }

    if (this.userRoles === undefined) {
      return false;
    }

    for (const role of roles) {
      if (this.userRoles.indexOf(role) > -1) {
        return true;
      }
    }

    return false;
  }

  canActivateChild (childRoute: ActivatedRouteSnapshot) {
    return this.canActivate(childRoute);
  }
}
