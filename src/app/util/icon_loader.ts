import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";

export class IconLoader {

    private readonly iconNames :string[] = [
        'account',
        'addedBy',
        'arrowDown',
        'arrowUp',
        'back',
        'bookmark',
        'bookmarks',
        'bin',
        'calculate',
        'cancel',
        'category',
        'changeLocation',
        'check',
        'city',
        'coin',
        'delete',
        'details',
        'discount',
        'edit',
        'filterOff',
        'filterOn',
        'fingerprint',
        'groceryList',
        'login',
        'logout',
        'menu',
        'products',
        'report',
        'save',
        'search',
        'settings',
        'shop',
        'stock'
    ];

    constructor(iconHolder :MatIconRegistry, domSanitizer: DomSanitizer) {
        for(let iconName of this.iconNames) {
            iconHolder.addSvgIcon(iconName.charAt(0).toUpperCase() + iconName.substring(1),
                domSanitizer.bypassSecurityTrustResourceUrl(`icons/${iconName}.svg`));
        }
    }

}
