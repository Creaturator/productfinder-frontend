export class DetailsValuesHolder {

    public id: string;
    public label: string;
    public value: any;

    constructor(id: string, label: string, value: any) {
        this.id = id;
        this.label = label;
        this.value = value;
    }

}