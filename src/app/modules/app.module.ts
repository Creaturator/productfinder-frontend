import {NgModule} from '@angular/core';
import {BrowserModule, DomSanitizer} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {BaseComponent} from '../components/base/base-component/base.component';
import {MatIconModule, MatIconRegistry} from "@angular/material/icon";
import {IconLoader} from "../util/icon_loader";
import {BaseSearchComponent} from "../components/base/base-search/base-search.component";
import {MatMenuModule} from "@angular/material/menu";
import {MatButton, MatButtonModule} from "@angular/material/button";
import {ConfirmDialogComponent} from "../components/confirm-dialog/confirm-dialog.component";
import {ProductSearchComponent} from "../pages/product-search/productSearch.component";
import {MatHeaderRow, MatHeaderRowDef, MatRow, MatRowDef, MatTableModule} from "@angular/material/table";
import {MatChipsModule} from "@angular/material/chips";
import {FilterBarComponent} from "../components/filter-bar/filter-bar.component";
import {MatCard, MatCardActions, MatCardContent} from "@angular/material/card";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatDialogModule} from "@angular/material/dialog";
import {DeniedComponent} from "../pages/denied/denied.component";
import {AuthConfig, OAuthModule, OAuthStorage} from "angular-oauth2-oidc";
import {HttpClientXsrfModule, provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {provideAnimationsAsync} from "@angular/platform-browser/animations/async";
import {MatInput, MatInputModule} from "@angular/material/input";
import {CommonModule, NgOptimizedImage} from "@angular/common";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatRadioModule} from "@angular/material/radio";
import {AppAuthService} from "../services/app.auth.service";
import {AppAuthGuard} from "../util/guard/app.auth.guard";
import {AppLoginComponent} from "../components/app-login/app-login.component";
import {TextTableCellComponent} from "../components/table-cells/text-table-cell/text-table-cell.component";
import {IsInRolesDirective} from "../directives/is-in-roles.dir";
import {CitySearchComponent} from "../pages/city-search/city-search.component";
import {CategoryDetailsComponent} from "../pages/details/category-details/category-details.component";
import {CityDetailsComponent} from "../pages/details/city-details/city-details.component";
import {ProductDetailsComponent} from "../pages/details/product-details/product-details.component";
import {StockDetailsComponent} from "../pages/details/stock-details/stock-details.component";
import {StoreDetailsComponent} from "../pages/details/store-details/store-details.component";
import {CityListComponent} from "../pages/lists/city-list/city-list.component";
import {StoreListComponent} from "../pages/lists/store-list/store-list.component";
import {CategoryListComponent} from "../pages/lists/category-list/category-list.component";
import {ProductListComponent} from "../pages/lists/product-list/product-list.component";
import {CityDisplayComponent} from "../components/city-display/city-display.component";
import {ButtonTableCellComponent} from "../components/table-cells/button-table-cell/button-table-cell.component";
import {ListHelpTextComponent} from "../components/list-help-text/list-help-text.component";
import {BooleanTableCellComponent} from "../components/table-cells/boolean-table-cell/boolean-table-cell.component";
import {BaseNonSearchComponent} from "../components/base/base-non-search/base-non-search.component";
import {CityEditComponent} from "../pages/edit/city-edit/city-edit.component";
import {CategoryEditComponent} from "../pages/edit/category-edit/category-edit.component";
import {StockEditComponent} from "../pages/edit/stock-edit/stock-edit.component";
import {ProductEditComponent} from "../pages/edit/product-edit/product-edit.component";
import {StoreEditComponent} from "../pages/edit/store-edit/store-edit.component";
import {CityDeleteComponent} from "../pages/delete/city-delete/city-delete.component";
import {StockDeleteComponent} from "../pages/delete/stock-delete/stock-delete.component";
import {StoreDeleteComponent} from "../pages/delete/store-delete/store-delete.component";
import {ProductDeleteComponent} from "../pages/delete/product-delete/product-delete.component";
import {CategoryDeleteComponent} from "../pages/delete/category-delete/category-delete.component";

/*
 * Copied from demo project
 */

export function storageFactory(): OAuthStorage {
    return sessionStorage;
}

@NgModule({
    declarations: [
        ProductSearchComponent,
        CitySearchComponent,
        DeniedComponent,

        CategoryDetailsComponent,
        CityDetailsComponent,
        ProductDetailsComponent,
        StockDetailsComponent,
        StoreDetailsComponent,

        CityListComponent,
        StoreListComponent,
        CategoryListComponent,
        ProductListComponent,

        CityEditComponent,
        CategoryEditComponent,
        StockEditComponent,
        ProductEditComponent,
        StoreEditComponent,

        CityDeleteComponent,
        StockDeleteComponent,
        StoreDeleteComponent,
        ProductDeleteComponent,
        CategoryDeleteComponent,

        BaseComponent,
        BaseSearchComponent,
        BaseNonSearchComponent,
        FilterBarComponent,
        ConfirmDialogComponent,
        AppLoginComponent,
        CityDisplayComponent,
        ListHelpTextComponent,

        TextTableCellComponent,
        ButtonTableCellComponent,
        BooleanTableCellComponent,

        IsInRolesDirective
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        CommonModule,

        MatFormFieldModule,
        MatInputModule,
        MatTableModule,
        MatIconModule,
        MatChipsModule,
        MatButtonModule,
        MatRadioModule,
        MatSelectModule,
        MatDialogModule,
        MatSnackBarModule,
        MatToolbarModule,
        MatMenuModule,
        MatCard,
        MatCardContent,
        MatCardActions,
        NgOptimizedImage,

        ReactiveFormsModule,

        HttpClientXsrfModule.withOptions({
            cookieName: 'XSRF-TOKEN',
            headerName: 'X-XSRF-TOKEN'
        }),
        OAuthModule.forRoot({
            resourceServer: {
                sendAccessToken: true
            }
        }),
        MatHeaderRow,
        MatRow,
        MatHeaderRowDef,
        MatRowDef,
        MatInput,
        MatButton,
    ],
    providers: [
        provideAnimationsAsync(),
        provideHttpClient(withInterceptorsFromDi()),
        AppAuthGuard,
        {
            provide: AuthConfig,
            useValue: AppAuthGuard.authConfig
        },
        {
            provide: OAuthStorage,
            useFactory: storageFactory
        },
    ],
    bootstrap: [BaseComponent]
})
export class AppModule {

    constructor(authService: AppAuthService, iconHolder: MatIconRegistry, domSanitizer: DomSanitizer) {
        new IconLoader(iconHolder, domSanitizer);
        authService.initAuth().finally();
    }

}
