import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProductSearchComponent} from "../pages/product-search/productSearch.component";
import {DeniedComponent} from "../pages/denied/denied.component";
import {CitySearchComponent} from "../pages/city-search/city-search.component";
import {CategoryListComponent} from "../pages/lists/category-list/category-list.component";
import {CityListComponent} from "../pages/lists/city-list/city-list.component";
import {ProductListComponent} from "../pages/lists/product-list/product-list.component";
import {StoreListComponent} from "../pages/lists/store-list/store-list.component";
import {StoreDetailsComponent} from "../pages/details/store-details/store-details.component";
import {ProductDetailsComponent} from "../pages/details/product-details/product-details.component";
import {CityDetailsComponent} from "../pages/details/city-details/city-details.component";
import {StockDetailsComponent} from "../pages/details/stock-details/stock-details.component";
import {CategoryDetailsComponent} from "../pages/details/category-details/category-details.component";
import {StoreEditComponent} from "../pages/edit/store-edit/store-edit.component";
import {ProductEditComponent} from "../pages/edit/product-edit/product-edit.component";
import {CityEditComponent} from "../pages/edit/city-edit/city-edit.component";
import {StockEditComponent} from "../pages/edit/stock-edit/stock-edit.component";
import {CategoryEditComponent} from "../pages/edit/category-edit/category-edit.component";
import {StoreDeleteComponent} from "../pages/delete/store-delete/store-delete.component";
import {ProductDeleteComponent} from "../pages/delete/product-delete/product-delete.component";
import {CityDeleteComponent} from "../pages/delete/city-delete/city-delete.component";
import {CategoryDeleteComponent} from "../pages/delete/category-delete/category-delete.component";

const titlePrefix: string = "Productfinder - ";

const routes: Routes = [
  {
    path: 'category/list',
    component: CategoryListComponent,
    title: titlePrefix + 'Categories'
  },
  {
    path: 'city/list',
    component: CityListComponent,
    title: titlePrefix + 'Cities'
  },
  {
    path: 'product/list',
    component: ProductListComponent,
    title: titlePrefix + 'Products'
  },
  {
    path: 'store/list',
    component: StoreListComponent,
    title: titlePrefix + 'Stores'
  },

  {
    path: 'store/details',
    component: StoreDetailsComponent,
    title: titlePrefix + 'Store Details'
  },
  {
    path: 'product/details',
    component: ProductDetailsComponent,
    title: titlePrefix + 'Product Details'
  },
  {
    path: 'city/details',
    component: CityDetailsComponent,
    title: titlePrefix + 'City Details'
  },
  {
    path: 'stock/details',
    component: StockDetailsComponent,
    title: titlePrefix + 'Stock Details'
  },
  {
    path: 'category/details',
    component: CategoryDetailsComponent,
    title: titlePrefix + 'Category Details'
  },

  {
    path: 'store/edit',
    component: StoreEditComponent,
    title: titlePrefix + 'Store Edit'
  },
  {
    path: 'product/edit',
    component: ProductEditComponent,
    title: titlePrefix + 'Product Edit'
  },
  {
    path: 'city/edit',
    component: CityEditComponent,
    title: titlePrefix + 'City Edit'
  },
  {
    path: 'stock/edit',
    component: StockEditComponent,
    title: titlePrefix + 'Stock Edit'
  },
  {
    path: 'category/edit',
    component: CategoryEditComponent,
    title: titlePrefix + 'Category Edit'
  },

  {
    path: 'store/delete',
    component: StoreDeleteComponent,
    title: titlePrefix + 'Store Delete'
  },
  {
    path: 'product/delete',
    component: ProductDeleteComponent,
    title: titlePrefix + 'Product Delete'
  },
  {
    path: 'city/delete',
    component: CityDeleteComponent,
    title: titlePrefix + 'City Delete'
  },
  {
    path: 'stock/delete',
    component: StockEditComponent,
    title: titlePrefix + 'Stock Delete'
  },
  {
    path: 'category/delete',
    component: CategoryDeleteComponent,
    title: titlePrefix + 'Category Delete'
  },

  {
    path: 'city/search',
    component: CitySearchComponent,
    title: titlePrefix + 'City Search',
  },
  {
    path: 'product/search',
    component: ProductSearchComponent,
    title: titlePrefix + 'Product search'
  },
  {
    path: '',
    redirectTo: 'city/search',
    pathMatch: 'full',
    title: 'Productfinder'
  },
  {
    path: 'noaccess',
    pathMatch: 'full',
    title: titlePrefix + 'Access denied',
    component: DeniedComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
