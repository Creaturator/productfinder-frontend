import {NamedModel} from "./namedModel";

export class Category extends NamedModel {
    public override id!: number;
    public override name!: string;
    public vegan!: boolean;
}