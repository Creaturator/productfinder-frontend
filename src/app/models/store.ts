import {City} from "./city";
import {NamedModel} from "./namedModel";

export class Store extends NamedModel {
    public override id!: number;
    public override name!: string;
    public address!: string;
    public city!: City;
}