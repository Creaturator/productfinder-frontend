import {Product} from "./product";
import {Store} from "./store";
import {Model} from "./model";

export class Stock extends Model {
    public override id!: number;
    public price!: number;
    public confirmationAmount!: number;
    public discount!: number;
    public product!: Product;
    public store!: Store;
}