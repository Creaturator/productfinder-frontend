import {Category} from "./category";
import {NamedModel} from "./namedModel";

export class Product extends NamedModel {
    public override id!: number;
    public override name!: string;
    public organic!: boolean;
    public nutriScore!: string;
    public price!: number;
    public category!: Category;
}