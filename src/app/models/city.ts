import {NamedModel} from "./namedModel";

export class City extends NamedModel {
    public override id!: number;
    public override name!: string;
    public alternateNames!: string;
    public alternateNameArray: string[] = [];
}