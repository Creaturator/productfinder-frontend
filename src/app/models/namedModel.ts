import {Model} from "./model";

export abstract class NamedModel extends Model {
    public readonly name: string | null = null;
    public override readonly id!: number;

}