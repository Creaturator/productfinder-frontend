import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {BaseComponent} from "../base/base-component/base.component";

/**
 * Copied from demo project
 */
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html'
})
export class ConfirmDialogComponent extends BaseComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    // @ts-ignore
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    super();
  }

  onConfirm(): void {
    this.dialogRef.close(true);
  }

  onDismiss(): void {
    this.dialogRef.close(false);
  }

}

export interface DialogData {
  title: string;
  message: string;
}
