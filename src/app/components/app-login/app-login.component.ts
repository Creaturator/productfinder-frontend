import {Component, OnInit} from '@angular/core';
import {AppAuthService} from '../../services/app.auth.service';
import {BaseComponent} from "../base/base-component/base.component";

/**
 * Copied from demo project
 */
@Component({
  selector: 'login-button',
  templateUrl: './app-login.component.html'
})
export class AppLoginComponent extends BaseComponent implements OnInit {

  username = ''
  useralias = ''

  constructor(
    private authService : AppAuthService
  ) {
    super();
  }

  ngOnInit(): void {
    this.authService.usernameObservable.subscribe(name => {
      this.username = name;
    });
    this.authService.useraliasObservable.subscribe(alias => {
      this.useralias = alias;
    });
  }

  public login () {
    this.authService.login()
  }

  public logout () {
    this.authService.logout()
  }

  public isAuthenticated () : boolean {
    return this.authService.isAuthenticated()
  }

}
