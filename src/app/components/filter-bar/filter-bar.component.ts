import {Component, Input} from '@angular/core';
import {NamedModel} from "../../models/namedModel";
import {BaseComponent} from "../base/base-component/base.component";

@Component({
    selector: 'filter-bar',
    templateUrl: './filter-bar.component.html',
    styleUrl: './filter-bar.component.less'
})
export class FilterBarComponent extends BaseComponent {

    @Input("options") public options!: NamedModel[];
    @Input("optionsName") public optionsName!: string;
    @Input('updateListFunc') public updateListFunc!: (input: NamedModel[]) => void;

    protected selectedFilters: NamedModel[] = [];

  public compareOptions(model1: NamedModel | null, model2: NamedModel | null): boolean {
    if (model1 === null || model2 === null) {
      console.error("one of the models is null");
      return false;
    }
      return model1.id === model2.id && model1.name === model2.name
  }

    protected notifyListUpdater(): void {
        this.updateListFunc(this.selectedFilters);
    }

}
