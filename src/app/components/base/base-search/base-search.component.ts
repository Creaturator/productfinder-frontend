import {Component, Input} from '@angular/core';
import {BaseComponent} from "../base-component/base.component";

@Component({
    selector: 'top-bar',
    templateUrl: './base-search.component.html',
    styleUrl: './base-search.component.less'
})
export class BaseSearchComponent extends BaseComponent {

    @Input("menuEntries") public menuEntries: string[][] = [];
    @Input("lookupFunc") public lookupFunc!: (name: string) => Promise<string[]>;

    private datalist: HTMLDataListElement;

    public constructor() {
        super();
        this.datalist = document.getElementById('resultList') as HTMLDataListElement;

        document.getElementById("searchField")?.addEventListener('change', (e) =>
            this.lookupFunc((e.currentTarget as HTMLInputElement).value)
                .then(this.fillDatalist));
    }

    private fillDatalist(productNames: string[]): void {
        this.datalist.innerHTML = '';
        for (let name of productNames) {
            let option: HTMLOptionElement = document.createElement('option');
            option.value = name;
            option.innerText = name;
            this.datalist.appendChild(option);
        }
    }

    protected goPageBack(): void {
        window.history.back();
    }

}
