import {Component, Input} from '@angular/core';
import {BaseComponent} from "../base-component/base.component";

@Component({
    selector: 'top-bar-no-search',
    templateUrl: './base-non-search.component.html',
    styleUrl: './base-non-search.component.less'
})
export class BaseNonSearchComponent extends BaseComponent {

    @Input('menuEntries') public menuEntries!: string[][];
  @Input('pagetitle') public pagetitle!: string;

  protected goPageBack(): void {
    window.history.back();
  }

}
