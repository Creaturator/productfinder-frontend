import {ComponentFixture, TestBed} from '@angular/core/testing';

import {BaseNonSearchComponent} from './base-non-search.component';

describe('BaseNonSearchComponent', () => {
    let component: BaseNonSearchComponent;
    let fixture: ComponentFixture<BaseNonSearchComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [BaseNonSearchComponent]
        })
            .compileComponents();

        fixture = TestBed.createComponent(BaseNonSearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
