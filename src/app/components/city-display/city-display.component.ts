import {Component, Input} from '@angular/core';
import {BaseComponent} from "../base/base-component/base.component";

@Component({
    selector: 'city-display',
    templateUrl: './city-display.component.html',
    styleUrl: './city-display.component.less'
})
export class CityDisplayComponent extends BaseComponent {

    @Input('city') cityName: string = '';

}
