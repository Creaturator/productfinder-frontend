import {Component, HostBinding, Input, OnDestroy, OnInit, Optional, ViewChild} from '@angular/core';
import {UserRoles} from "../../../util/guard/user_roles";
import {MatCellDef, MatColumnDef, MatFooterCellDef, MatHeaderCellDef, MatTable} from "@angular/material/table";
import {BaseComponent} from "../../base/base-component/base.component";
import {NamedModel} from "../../../models/namedModel";

@Component({
    selector: 'button-table-cell',
    templateUrl: './button-table-cell.component.html',
    styleUrl: './button-table-cell.component.less'
})
export class ButtonTableCellComponent extends BaseComponent implements OnInit, OnDestroy {

    @Input('item') public item!: NamedModel;
    @Input('urlPrefix') public urlPrefix: string = '';

    @Optional()
    public table: MatTable<unknown>
    @HostBinding('attr.ariaHidden')
    ariaHidden!: true;
    @HostBinding('class')
    classes!: 'column-template cdk-visually-hidden';
    @ViewChild(MatColumnDef, {static: true})
    columnDef!: MatColumnDef;
    @ViewChild(MatCellDef, {static: true})
    cellDef!: MatCellDef;
    @ViewChild(MatHeaderCellDef, {static: true})
    headerCellDef!: MatHeaderCellDef;
    @ViewChild(MatFooterCellDef, {static: true})
    footerCellDef!: MatFooterCellDef;
    protected readonly UserRoles = UserRoles;

    // @ts-ignore
    constructor(table: MatTable<unknown>) {
        super();
        this.table = table;
    }

    ngOnInit(): void {
        if (this.table && this.columnDef) {
            this.columnDef.name = 'buttons';
            this.columnDef.cell = this.cellDef;
            this.columnDef.headerCell = this.headerCellDef;
            this.columnDef.footerCell = this.footerCellDef;
            this.table.addColumnDef(this.columnDef);
        }
    }

    ngOnDestroy(): void {
        if (this.table) {
            this.table.removeColumnDef(this.columnDef);
        }
    }
}
