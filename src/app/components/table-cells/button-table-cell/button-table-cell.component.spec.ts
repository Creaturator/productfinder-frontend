import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ButtonTableCellComponent} from './button-table-cell.component';

describe('ButtonTableCellComponent', () => {
    let component: ButtonTableCellComponent;
    let fixture: ComponentFixture<ButtonTableCellComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ButtonTableCellComponent]
        })
            .compileComponents();

        fixture = TestBed.createComponent(ButtonTableCellComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
