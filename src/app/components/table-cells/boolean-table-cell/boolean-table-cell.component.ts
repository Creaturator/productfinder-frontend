import {Component, HostBinding, Input, OnDestroy, OnInit, Optional, ViewChild} from '@angular/core';
import {MatCellDef, MatColumnDef, MatFooterCellDef, MatHeaderCellDef, MatTable} from "@angular/material/table";
import {BaseComponent} from "../../base/base-component/base.component";

@Component({
    selector: 'boolean-table-cell',
    templateUrl: './boolean-table-cell.component.html',
    styleUrl: './boolean-table-cell.component.less'
})
export class BooleanTableCellComponent extends BaseComponent implements OnInit, OnDestroy {

    @Input('varName')
    public varName!: string;
    @Input('columnName')
    public columnName!: string;
    @Input('value')
    public value!: boolean;

    @Optional()
    public table: MatTable<unknown>
    @HostBinding('attr.ariaHidden')
    ariaHidden!: true;
    @HostBinding('class')
    classes!: 'column-template cdk-visually-hidden';
    @ViewChild(MatColumnDef, {static: true})
    columnDef!: MatColumnDef;
    @ViewChild(MatCellDef, {static: true})
    cellDef!: MatCellDef;
    @ViewChild(MatHeaderCellDef, {static: true})
    headerCellDef!: MatHeaderCellDef;
    @ViewChild(MatFooterCellDef, {static: true})
    footerCellDef!: MatFooterCellDef;

    // @ts-ignore
    constructor(table: MatTable<unknown>) {
        super();
        this.table = table;
    }

    ngOnInit(): void {
        if (this.table && this.columnDef) {
            this.columnDef.name = this.varName;
            this.columnDef.cell = this.cellDef;
            this.columnDef.headerCell = this.headerCellDef;
            this.columnDef.footerCell = this.footerCellDef;
            this.table.addColumnDef(this.columnDef);
        }
    }

    ngOnDestroy(): void {
        if (this.table) {
            this.table.removeColumnDef(this.columnDef);
        }
    }

}
