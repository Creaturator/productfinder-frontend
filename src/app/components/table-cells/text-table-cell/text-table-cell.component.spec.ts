import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TextTableCellComponent} from './text-table-cell.component';

describe('TextTableCellComponent', () => {
    let component: TextTableCellComponent;
    let fixture: ComponentFixture<TextTableCellComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [TextTableCellComponent]
        })
            .compileComponents();

        fixture = TestBed.createComponent(TextTableCellComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
