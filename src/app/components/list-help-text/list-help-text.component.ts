import {Component, Input} from '@angular/core';
import {BaseComponent} from "../base/base-component/base.component";

@Component({
    selector: 'list-help-text',
    templateUrl: './list-help-text.component.html',
    styleUrl: './list-help-text.component.less'
})
export class ListHelpTextComponent extends BaseComponent {

    @Input('itemType') public itemType: string = '';

}
