import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ListHelpTextComponent} from './list-help-text.component';

describe('ListHelpTextComponent', () => {
    let component: ListHelpTextComponent;
    let fixture: ComponentFixture<ListHelpTextComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [ListHelpTextComponent]
        })
            .compileComponents();

        fixture = TestBed.createComponent(ListHelpTextComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
